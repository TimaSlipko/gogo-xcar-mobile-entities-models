package models

import (
	"gitlab.com/TimaSlipko/gogo-xcar-mobile-entities-models/entity"
	"gitlab.com/TimaSlipko/gogo-xcar-mobile-entities-models/helpers"
)

type DriverPassengerChat struct {
	ID          uint `json:"id"`
	DriverID    uint `json:"driver_id"`
	PassengerID uint `json:"passenger_id"`
	OrderID     uint `json:"order_id"`
	IsArchived  bool `json:"is_archived"`
}

func GetDriverPassengerChat(driverPassengerChat entity.DriverPassengerChat) DriverPassengerChat {
	var driverPassengerChatModel DriverPassengerChat

	driverPassengerChatModel.ID = driverPassengerChat.ID
	driverPassengerChatModel.DriverID = driverPassengerChat.DriverID
	driverPassengerChatModel.PassengerID = driverPassengerChat.PassengerID
	driverPassengerChatModel.OrderID = driverPassengerChat.OrderID
	driverPassengerChatModel.IsArchived = helpers.Uint8ToBool(driverPassengerChat.IsArchived)

	return driverPassengerChatModel
}

func GetDriverPassengerChatArray(array []entity.DriverPassengerChat) []DriverPassengerChat {
	var arrayModel []DriverPassengerChat

	for _, arrayElement := range array {
		arrayModel = append(arrayModel, GetDriverPassengerChat(arrayElement))
	}

	return arrayModel
}
