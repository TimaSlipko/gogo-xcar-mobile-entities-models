package models

import (
	"gitlab.com/TimaSlipko/gogo-xcar-mobile-entities-models/entity"
)

type User struct {
	ID          uint    `json:"id"`
	Name        string  `json:"name"`
	Role        string  `json:"role"`
	ProfileUrl  string  `json:"profile_url"`
	Platform    string  `json:"platform"`
	DeviceToken *string `json:"device_token"`
	MqttToken   string  `json:"mqtt_token"`
}

func GetUser(user entity.User) User {
	var userModel User

	userModel.ID = user.ID
	userModel.Name = user.Name
	userModel.Role = user.Role
	userModel.ProfileUrl = user.ProfileUrl
	userModel.Platform = user.Platform
	userModel.DeviceToken = user.DeviceToken
	userModel.MqttToken = user.MqttToken

	return userModel
}

func GetUserArray(array []entity.User) []User {
	var arrayModel []User

	for _, arrayElement := range array {
		arrayModel = append(arrayModel, GetUser(arrayElement))
	}

	return arrayModel
}
