package models

import (
	"gitlab.com/TimaSlipko/gogo-xcar-mobile-entities-models/entity"
)

type DriverPassengerChatMessage struct {
	ID        uint    `json:"id"`
	ChatID    uint    `json:"chat_id"`
	UserID    uint    `json:"user_id"`
	Timestamp int     `json:"timestamp"`
	Message   *string `json:"message"`
	ImageUrl  *string `json:"image_url"`
	AudioUrl  *string `json:"audio_url"`
}

func GetDriverPassengerChatMessage(driverPassengerChatMessage entity.DriverPassengerChatMessage) DriverPassengerChatMessage {
	var driverPassengerChatMessageModel DriverPassengerChatMessage

	driverPassengerChatMessageModel.ID = driverPassengerChatMessage.ID
	driverPassengerChatMessageModel.ChatID = driverPassengerChatMessage.ChatID
	driverPassengerChatMessageModel.UserID = driverPassengerChatMessage.UserID
	driverPassengerChatMessageModel.Timestamp = driverPassengerChatMessage.Timestamp
	driverPassengerChatMessageModel.Message = driverPassengerChatMessage.Message
	driverPassengerChatMessageModel.ImageUrl = driverPassengerChatMessage.ImageUrl
	driverPassengerChatMessageModel.AudioUrl = driverPassengerChatMessage.AudioUrl

	return driverPassengerChatMessageModel
}

func GetDriverPassengerChatMessageArray(array []entity.DriverPassengerChatMessage) []DriverPassengerChatMessage {
	var arrayModel []DriverPassengerChatMessage

	for _, arrayElement := range array {
		arrayModel = append(arrayModel, GetDriverPassengerChatMessage(arrayElement))
	}

	return arrayModel
}
