package entity

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID          uint
	Name        string `gorm:"type:text"`
	Role        string `gorm:"type:text"`
	ProfileUrl  string  `gorm:"type:text"`
	Platform    string  `gorm:"type:varchar(7)"`
	DeviceToken *string `gorm:"type:text"`
	MqttToken   string `gorm:"type:text"`
}
