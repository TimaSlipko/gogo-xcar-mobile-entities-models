package entity

import (
	"gorm.io/gorm"
)

type DriverPassengerChatMessage struct {
	gorm.Model
	ID        uint
	ChatID    uint
	UserID    uint
	Timestamp int
	Message   *string `gorm:"type:text"`
	ImageUrl  *string `gorm:"type:text"`
	AudioUrl  *string `gorm:"type:text"`
}
