package entity

import (
	"gorm.io/gorm"
)

type DriverPassengerChat struct {
	gorm.Model
	ID          uint
	DriverID    uint
	PassengerID uint
	OrderID     uint `gorm:"unique"`
	IsArchived  uint8
}
